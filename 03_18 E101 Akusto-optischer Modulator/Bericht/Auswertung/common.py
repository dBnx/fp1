#!/bin/python3 

import os # To read the cwd
import numpy as np
import pandas as pd
import scipy.constants as c
import matplotlib.pyplot as plt
import uncertainties.unumpy as unp 
from scipy.optimize import curve_fit
from uncertainties import ufloat 
from uncertainties import ufloat_fromstr
from uncertainties.unumpy import nominal_values as nom
from uncertainties.unumpy import std_devs as dev

# ┌──────────────────────────┐
# │ General helper functions │
# └──────────────────────────┘
def umean( array ):
    w = 1.0 / unp.std_devs( array )**2
    wsum = np.sum( w )
    mean = np.sum( w * unp.nominal_values(array) ) / wsum
    mean_unc = np.sqrt( 1.0 / wsum )
    return ufloat( np.mean( unp.nominal_values(array) ), mean_unc )

def get_fit_parameters( func, x, y, init=None, bounds=None ):
    popt, pcov = curve_fit( func,
            xdata=x, 
            ydata=y,
            p0=init,
            bounds=bounds,
            maxfev=100000) if init is not None and bounds is not None else \
        curve_fit( func,
            xdata=x, 
            ydata=y,
            p0=init,
            maxfev=100000) if bounds is None else\
        curve_fit( func,
            xdata=x, 
            ydata=y,
            maxfev=100000) # if init is None
    perr = np.sqrt(np.diag(pcov))
    return popt, *[ ufloat( popt[i], perr[i] ) for i in range(len(popt)) ]

#def get_fit_parameters( func, x,y):
#    popt, pcov = curve_fit( func,
#            xdata=x, 
#            ydata=y,
#            maxfev=100000)
#    perr = np.sqrt(np.diag(pcov))
#    return popt, *[ ufloat( popt[i], perr[i] ) for i in range(len(popt)) ]

# ┌────────────────────────┐
# │ Other helper functions │
# └────────────────────────┘
def Ticks_to_Angle( t ): # Rad
    # TODO: 
    # Website: 5 arcmin precision
    # - 0.08333° ~ 1.454 mrad 
    # 25 Ticks  / Umdrehung - Annahme 1° ( Marker in Datenblatt )
    # - 0.04° ~ 0.6981 mrad 
    return t * 1.1454e-3 

def Vpp_to_W( vpp ):
    G = 47.0 # TODO
    R = 50.0 # Ohm
    return ( G * vpp  )**2 / ( 2 * R ) / 4 # / 4 ist ein fix TODO

Pbg = ufloat( -1.88, 0.1) *1E-6

# ┌────────────────┐
# │ Insertion Loss │
# └────────────────┘
d_il = pd.read_csv(os.getcwd() + "/d_insertion_loss.csv")

IL_l_A  = np.array([ ufloat_fromstr( v ) for v in d_il['PlA']  ]) *1E-3
IL_bg_A = np.array([ ufloat_fromstr( v ) for v in d_il['PbgA'] ]) *1E-6
IL_l_B  = np.array([ ufloat_fromstr( v ) for v in d_il['PlB']  ]) *1E-3
IL_bg_B = np.array([ ufloat_fromstr( v ) for v in d_il['PbgB'] ]) *1E-6
IL_A = IL_l_A - IL_bg_A
IL_B = IL_l_B - IL_bg_B
IL = 1 - IL_A / IL_B
ref_P_out = umean(IL_A) # TODO?


# ┌────────────────┐
# │ Bragg Winkel   │
# └────────────────┘
d_bragg = pd.read_csv(os.getcwd() + "/d_bragg_winkel.csv")

BR_vpp = np.array([                 v   for v in d_bragg['Vpp']  ]) *1E-3
BR_0_l = np.array([ ufloat_fromstr( v ) for v in d_bragg['P0']   ]) *1E-6
BR_0bg = np.array([ ufloat_fromstr( v ) for v in d_bragg['P0bg'] ]) *1E-6
BR_1_l = np.array([ ufloat_fromstr( v ) for v in d_bragg['P1']   ]) *1E-6
BR_1bg = np.array([ ufloat_fromstr( v ) for v in d_bragg['P1bg'] ]) *1E-6

BR_RF = Vpp_to_W( BR_vpp )
BR_0  = BR_0_l - BR_0bg
BR_1  = BR_1_l - BR_1bg


# ┌────────────────┐
# │ Frequenz       │
# └────────────────┘
d_freq = pd.read_csv(os.getcwd() + "/d_freq.csv")

FR_f   = np.array([                 v   for v in d_freq['Frequenz'] ]) *1E6  # Hz
FR_d   = np.array([ ufloat_fromstr( v ) for v in d_freq['Distanz']  ]) *1E-2 # metres
FR_1_l = np.array([ ufloat_fromstr( v ) for v in d_freq['P1']       ]) *1E-3
FR_1bg = np.array([ ufloat_fromstr( v ) for v in d_freq['Pbg']      ]) *1E-6

FR_1 = FR_1_l - FR_1bg

alpha = ufloat( 34.0, 0.3 ) * 1e-2 # metres
beta  = ufloat( 22.3, 0.3 ) * 1e-2 # metres
gamma = ufloat( 62.6, 0.5 ) * 1e-2 # metres
long_side_length = alpha + beta + gamma

def bragg_angle_from_distance( d ):
    return unp.arctan2( d , long_side_length ) / 2


# ┌────────────────┐
# │ Winkel         │
# └────────────────┘
d_angle = pd.read_csv(os.getcwd() + "/d_winkel.csv")

AN_angle = np.array([                 v   for v in d_angle['Winkel']]) *1E-3
AN_1_l   = np.array([ ufloat_fromstr( v ) for v in d_angle['P1']      ]) *1E-6 # TODO?
AN_1bg   = np.array([ ufloat_fromstr( v ) for v in d_angle['Pbg']     ]) *1E-6
AN_d     = np.array([ ufloat_fromstr( v ) for v in d_angle['Distanz' ]]) *1E-3

AN_1 = AN_1_l - AN_1bg

# ┌────────────────┐
# │ Debugging      │
# └────────────────┘
if __name__ == "__main__":
    print(f"{d_il.head()=}\n")
    print(f"{d_il['PlA']=}\n")
    print(f"{IL_A=}\n")
    print(f"{IL_B=}\n")
    print(f"{IL=}\n")





