#!/bin/python3 

from common import *

# TODO Fix
# TODO Fit

# ┌───────────┐
# │ Used data │
# └───────────┘
#P_out = BR_RF
P_0 = BR_0
P_1 = BR_1
P_out = P_0 + P_1 # TODO
P_RF = BR_RF
e_measured = P_1 / P_out 

# TODO Fix
# TODO Fit with sinc**2
print(f"{P_1=}")
print(f"{P_out=}")


# ┌─────┐
# │ Fit │
# └─────┘
def fit( Prf, e_0, Psat  ):
    return e_0 * np.sin(c.pi / 2 * np.sqrt( Prf / Psat ) )**2

popt, e_0, psat  = get_fit_parameters( fit, nom( P_RF ), nom( e_measured ) )
print(f"{e_0=:.1uS}")
print(f"{psat=:.1uS}")
#print(f"{shift=:.1uS}")


# ┌──────┐
# │ Plot │
# └──────┘
tfit = np.linspace(P_RF[0], P_RF[-1], 1000)

plt.plot(tfit, fit(tfit, *popt), 
        label="Fit: " r"$A \, \mathrm{sin}(\frac{\pi}{2}\sqrt{ \frac{P_{RF}}{B} } )^2$ mit:" "\n"
        r"$A$  = " f" {e_0:.1uS} " "\n"
        r"$B$ = " f"{psat:.2uS} " "\n"
        #r"$C$ = " f"{shift:.2uS} " # TODO: Units
        )

plt.errorbar( nom( P_RF ),
        nom( e_measured ),
        yerr=dev( e_measured ),
        ls='',
        marker='|',
        capsize=5, 
        label="Gemessen" )
plt.ylabel(r"Beugungseffizienz")
plt.xlabel(r"RF-Leistung in $W$")
#plt.yscale("log")
#plt.xscale("log")
plt.legend()
plt.grid()

plt.savefig("../s_beugungseffizienz.png")
plt.show()



