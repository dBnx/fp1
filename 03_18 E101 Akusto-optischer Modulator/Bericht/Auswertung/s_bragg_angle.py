#!/bin/python3 

from common import *

# ┌───────────┐
# │ Used data │
# └───────────┘
P_RF = BR_RF
P_0  = BR_0
P_1  = BR_1

# ┌─────┐
# │ Fit │
# └─────┘



# ┌──────┐
# │ Plot │
# └──────┘
def plot( label, data ):
    plt.errorbar( P_RF,
            nom( data ),
            yerr=dev( data ),
            ls='',
            marker='|',
            capsize=5, 
            label=label )
    plt.ylabel(r"Leistung in $mW$")
    plt.xlabel(r"RF-Leistung in $W$")
    #plt.yscale("log")
    #plt.xscale("log")
    plt.legend()

plot( r"0. Ordnung", P_0 )
plt.savefig("../s_bragg_angle_0.png")
#plt.show( block=False )
plt.show()

plt.clf()
plot( r"1. Ordnung", P_1 )
plt.savefig("../s_bragg_angle_1.png")
plt.show()

#tfit = np.linspace(ch[0], ch[-1], 1000)
#
#plt.plot(tfit, fit(tfit, *popt), label="Fit: " r"$R_\textrm{Bg} + \frac{A}{(d' - H)^2}$ mit:" "\n"
#        r"$A$  = " f" {fit_R:.1uS} Bq mm$^2$" "\n"
#        r"$H$ = " f"{fit_H:.2uS} mm" "\n"
#        r"$R_\textrm{Bg}$ = " f"{-fit_B:.2uS} Bq")





