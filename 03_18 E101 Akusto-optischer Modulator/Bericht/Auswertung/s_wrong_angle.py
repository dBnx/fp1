#!/bin/python3 
from common import *

# TODO: Text oder Bars: Fehler im Winkel und in Umrechnung nicht beachtet ! 

# ┌───────────┐
# │ Used data │
# └───────────┘
P_1   = AN_1
W     = Ticks_to_Angle( AN_angle )
Dist  = AN_d
P_out = ref_P_out
e_measured = P_1 / P_out

#print(f"{P_1=:.1uS}")
#print(f"{AN_1bg=:.1uS}")
print(f"{ref_P_out=:.1uS}")


# ┌─────┐
# │ Fit │
# └─────┘
def fit( delta, e_0, Q ):
    Delta = delta # / Bragg # TODO
    return e_0 * np.sinc(Q/4 * Delta)**2

popt, e_0, Q = get_fit_parameters( fit, nom( W ), nom( e_measured ), init=[ 0.36, -4.2], 
        bounds=([0, -np.inf],[np.inf, np.inf]) )
print(f"{e_0=:.1uS}")
print(f"{Q=:.1uS}")
#print(f"{f0=:.1uS}")
#print(f"{shift=:.1uS}")


# ┌──────┐
# │ Plot │
# └──────┘
tfit = np.linspace(nom(W[0]), nom(W[-1]), 1000) # TODO Winkel und nicht W

plt.plot(tfit, fit(tfit, *popt), 
            label="Fit: " r"$A \, \mathrm{sinc}(\frac{B}{2} \Delta )^2$ mit:" "\n"
        r"$A$  = " f" {e_0:.1uS} " "\n"
        r"$B$ = " f"{Q:.2uS} " "\n"
        #r"$C$ = " f"{shift:.2uS} " # TODO: Units
        )

plt.errorbar( nom( W ),
        nom( e_measured ),
        yerr=dev( e_measured ),
        ls='',
        marker='|',
        capsize=5, 
        label="Gemessen" )
plt.ylabel(r"Beugungseffizienz")
plt.xlabel(r"Winkel in rad")
plt.legend()

plt.grid()
plt.savefig("../s_beugungseffizienz_angle.png")
plt.show()



