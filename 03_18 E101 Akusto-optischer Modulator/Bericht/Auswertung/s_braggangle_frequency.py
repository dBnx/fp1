#!/bin/python3 

from common import *

# TODO: Winkel um const. Faktor 1e4 zu klein

# ┌───────────┐
# │ Used data │
# └───────────┘
wavelength = 632.8e-9 # meters - HeNe-Laser

f_RF = FR_f
side_lengths = FR_d
bragg_angle = bragg_angle_from_distance( side_lengths )

print(f"{long_side_length=:.1uS}")
print(f"{side_lengths[0]=:.1uS}")


# ┌─────┐
# │ Fit │
# └─────┘
#def fit( f_RF, v_s ):
#    return wavelength * f_RF / v_s
#
#popt, e_0, Q = get_fit_parameters( fit, nom( W ), nom( e_measured ), init=[ 0.36, -4.2] )
#print(f"{e_0=:.1uS}")
#print(f"{Q=:.1uS}")
#print(f"{f0=:.1uS}")
#print(f"{shift=:.1uS}")


# ┌──────┐
# │ Plot │
# └──────┘
#tfit = np.linspace(nom(W[0]), nom(W[-1]), 1000) # TODO Winkel und nicht W
#
#plt.plot(tfit, fit(tfit, *popt), 
#            label="Fit: " r"$A \, \mathrm{sinc}(\frac{B}{2} \Delta )^2$ mit:" "\n"
#        r"$A$  = " f" {e_0:.1uS} " "\n"
#        r"$B$ = " f"{Q:.2uS} " "\n"
#        #r"$C$ = " f"{shift:.2uS} " # TODO: Units
#        )

bragg_angle = bragg_angle * 1E3 # mrad
plt.errorbar( nom( f_RF ) * 1E-6,
        nom( bragg_angle ),
        yerr=dev( bragg_angle ),
        ls='',
        marker='+',
        capsize=5, 
        label="Gemessen" )
plt.ylabel(r"Bragg-Winkel in mrad")
plt.xlabel(r"RF-Frequenz in MHz")
#plt.legend()

plt.grid()
plt.savefig("../s_bragg_angle_frequency.png")
plt.show()



