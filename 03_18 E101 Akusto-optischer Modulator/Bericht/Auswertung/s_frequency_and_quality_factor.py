#!/bin/python3 

from common import *

# TODO Fix
# TODO Fit

# ┌───────────┐
# │ Used data │
# └───────────┘
# Measured curve
P_out = 1#FR_0 + FR_1 
P_1 = FR_1
e_measured = P_1 / P_out 

#f_0 = 80e6 # 80 MHz
f = FR_f

#print(f"{len(F)=}")
print(f"{len(P_1)=}")
print(f"{len(e_measured)=}")

# ┌─────┐
# │ Fit │
# └─────┘
def fit( f, e_0, Q, f0):
    F = f/f0
    return e_0 * np.sinc(Q/4 * F * (1-F) )**2

popt, e_0, Q, f0 = get_fit_parameters( fit, nom( f ), nom( e_measured ) )
print(f"{e_0=:.1uS}")
print(f"{Q=:.1uS}")
print(f"{f0=:.1uS}")
#print(f"{shift=:.1uS}")


# ┌──────┐
# │ Plot │
# └──────┘
F = f/f0
tfit = np.linspace(nom(f[0]), nom(f[-1]), 1000)

plt.plot(tfit, fit(tfit, *popt), 
        label="Fit: " r"$A \, \mathrm{sinc}(\frac{\pi}{2}\sqrt{ \frac{P_{RF}}{B} } + C)^2$ mit:" "\n"
        #r"$A$  = " f" {e_0:.1uS} " "\n"
        #r"$B$ = " f"{psat:.2uS} " "\n"
        #r"$C$ = " f"{shift:.2uS} " # TODO: Units
        )

plt.errorbar( nom( f ),
        nom( e_measured ),
        yerr=dev( e_measured ),
        ls='',
        marker='|',
        capsize=5, 
        label="Gemessen" )
plt.ylabel(r"Beugungseffizienz")
plt.xlabel(r"RF-Leistung in $W$")
#plt.yscale("log")
#plt.xscale("log")
plt.legend()


plt.savefig("../s_beugungseffizienz_freq.png")
plt.show()



