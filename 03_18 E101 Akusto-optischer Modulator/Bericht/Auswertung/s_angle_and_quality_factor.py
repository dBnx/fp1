#!/bin/python3 

from common import *

# TODO Fix
# TODO Fit

# ┌───────────┐
# │ Used data │
# └───────────┘
# Measured curve
P_out = BR_0 + BR_1 
P_1 = BR_1
e_measured = P_1 / P_out 

# Detuning for the fit
f_0 = 80e6 # 80 MHz
delta = FR_f - f_0


# ┌─────┐
# │ Fit │
# └─────┘



# ┌──────┐
# │ Plot │
# └──────┘
plt.errorbar( nom( P_out ),
        nom( e_measured ),
        yerr=dev( e_measured ),
        ls='',
        marker='|',
        capsize=5, 
        label="Gemessen" )
plt.ylabel(r"Beugungseffizienz")
plt.xlabel(r"RF-Leistung in $W$")
#plt.yscale("log")
#plt.xscale("log")
plt.legend()


plt.savefig("../s_beugungseffizienz_angle_and_q.png")
plt.show()



